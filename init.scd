Forest.mainLevel = 15.dbamp;
~forest = Forest.new;

~chris = NetAddr.new("25.1.107.255", 8000);
~chris.sendMsg();

// 8-point demonstration
(
~mupssong = ForestMupsSong.new( db: 0, threshold: 0.06 ).play;
"start mupssong".postln;
)

~mupssong.free; "free mupssong".postln;

// send altitudes for plane
~sendAltitudes = ForestSendAltitudeData.new; "sendAltitudes on".postln;
~arvo = ForestArvo.new( db: 16, threshold: 0.03 ).play( 9, 13 );

~arvo.free;
~sendAltitudes.free; "sendAltitudes off".postln;


(
// perlin noise
~avgDeltas = ForestAverageDeltas.new;
~avgDeltas.smoothingFactor = 0.9;
~avgDeltas.scalingFactor = 6;
"avgDeltas on".postln;
)

(
~reactiveEq = ForestReactiveEq.new( threshold: 0.06 ).play;

OSCdef(\reactiveEq, {|msg|
    var data = msg[1..];
    data = data.resamp1(256);
    // data.postln;
    ~reactiveEq.fxChain.fx[\pvEq].set(\magnitudes, Ref( data ));
}, '/hf-height')
)

// free it up
(
OSCdef(\reactiveEq, {});
~avgDeltas.free;
~reactiveEq.free;
)
