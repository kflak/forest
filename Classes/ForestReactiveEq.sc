ForestReactiveEq : ForestDeltaTrig {

    var currentPos = 0;
    var initTime;

    *new{|db=0, speedlim=0.5, threshold=0.01, minAmp= -20, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestReactiveEq;
    }

    initForestReactiveEq{
        initTime = thisThread.seconds;
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\pvEq);
        fxChain.add(\compressor, [
            \ratio, 8,
            \thresh, -30.dbamp,
            \amp, 12.dbamp,
            ]);
        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, 0.3,
            ]);
    }


    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|

            var buf = Forest.buf[\verkstedshallen].choose;
            var numFrames = buf.numFrames;
            var dur = 0.2;
            var step = dur * Forest.server.sampleRate;
            var len = dt.linlin(0.0, 1.0, step, step * 10);
            var pos = (currentPos, currentPos+step..currentPos+len);
            currentPos = pos[pos.size-1].mod(numFrames);

            Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, dur,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 4,
                \startPos, Pseq(pos),
                \legato, 2,
                \rate, Pwhite(0.25, 1.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
