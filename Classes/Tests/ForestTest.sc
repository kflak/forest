ForestTest1 : UnitTest {
	test_check_classname {
		var result = Forest.new;
		this.assert(result.class == Forest);
	}
}

ForestSetNumInputBusChannelsTest : UnitTest {
    var b;
    setUp{
        b = Forest.new;
    }
	test_set_num_inputbuses {
        var b = Forest.new;
		this.assert(Server.default.options.numInputBusChannels == 8);
	} }

ForestSetNumOutputBusChannelsTest : UnitTest {
    var b;
    setUp {
       Forest.numSpeakers = 4;
       Forest.numSubs = 1;
       b = Forest.new;
    }

	test_set_num_outputbuses {
		var result = Server.default.options.numOutputBusChannels;
		this.assert(result == 5);
        postf("numOutputBusChannels %\n", result);
	}
}

ForestSetLatencyTest : UnitTest {
	test_set_latency {
		var result = Server.default.latency;
		this.assert(result == 0.1);
	}
}

ForestBootServerTest : UnitTest {
    test_boot_server {
        Server.default.sync;
        this.assert(Server.default.serverRunning);
    }
}

ForestSynthDefTest : UnitTest {
    test_synth_defs {
        var result;
        Server.default.sync;
        result = SynthDescLib.getLib(\global).at(\sine).isNil;
        this.assert(result == false);
    }
}

ForestTester {
	*new {
		^super.new.init();
	}

	init {
		ForestTest1.run;
        ForestSetNumInputBusChannelsTest.run;
        ForestSetNumOutputBusChannelsTest.run;
        ForestSetLatencyTest.run;
        ForestBootServerTest.run;
        ForestSynthDefTest.run;
	}
}
