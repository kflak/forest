ForestShuffleIsClassTest : UnitTest{
	test_check_classname {
		var result = ForestShuffle.new;
		this.assert(result.class == ForestShuffle);
	}
}

ForestShuffleCreateMBDeltaTrigTest : UnitTest {
    test_create_mbdeltatrig {
        var b = ForestShuffle.new;
        var result = b.mbDeltaTrigs;
        this.assert(result.isCollection && result[0].class == MBDeltaTrig);
    }
}

ForestShuffleSetInitTimeTest : UnitTest {
    test_set_init_time {
        var b = ForestShuffle.new;
        var result = b.initTime;
        this.assert(result == thisThread.seconds);
    }
}

ForestShuffleTester {
	*new {
		^super.new.init();
	}

	init {
        ForestShuffleIsClassTest.run;
        ForestShuffleCreateMBDeltaTrigTest.run;
        ForestShuffleSetInitTimeTest.run;
	}
}
