ForestPercussive : ForestDeltaTrig {
    var <>quantize = false;
    var <clock;
    var <pbind;
    var <>kit = \kickWater;
    var <>quant = 1;
    var <>length = 1;
    var <>minRevMix = 0.0;
    var <>maxRevMix = 0.6;
    var <>minRevTime = 0.3;
    var <>maxRevTime = 3;
    var <>locut = 80;
    var <>tempo = 1;
    var pdef;

    *new{|db=0, speedlim=1, threshold=0.03, minAmp= -60, maxAmp= -20, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestPercussive;
    }

    initForestPercussive {
        clock = TempoClock.new();
        this.prAddFx;
        this.prCreatePdef;
        this.prCreatePatterns;
        freeMBsImmediately = false;
    }

    prAddFx {
        fxChain.add(\eq, [
            \locut, locut,
        ]);
        fxChain.add(\comb, [
            \delay, Pwhite(0.002, 0.007),
            \decay, 1,
            \lag, 1,
            \mix, Pwhite(0.0, 0.15),
            ]);
        fxChain.addPar(
            \comb, [\mix, 0.4, \delay, 0.2, \decay, 1, \amp, 1/3],
            \comb, [\mix, 0.4, \delay, 0.5, \decay, 1, \amp, 1/3],
            \comb, [\mix, 0.4, \delay, 0.7, \decay, 1, \amp, 1/3],
        );
        fxChain.add(\jpverb, [
            \revtime, Pwhite(minRevTime, maxRevTime),
            \revmix, Pwhite(minRevMix, maxRevMix),
        ]);
    }

    prCreatePdef {
        pdef = rand(1000000).asSymbol;
        pdef.clock = clock;
        Pdef(pdef).quant_(quant).play;
    }

    prCreatePatterns {

        pbind = ();

        pbind[\panPercLow] = Pbind(
            \instrument, \playbufPerc,
            \dur, Prand([1/8, 1/4], inf),
            \buf, Prand(Forest.buf[\panPercLow], inf ),
            \db, Pwhite(-6, 0),
            \loop, 0,
            \attack, 0,
            \release, Pkey(\dur) * 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
        pbind[\panPercMid] = Pbind(
            \instrument, \playbufPerc,
            \dur, Prand([1/8, 1/4], inf),
            \buf, Prand(Forest.buf[\panPercMid], inf ),
            \db, Pwhite(-6, 0),
            \loop, 0,
            \attack, 0,
            \release, Pkey(\dur) * 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
        pbind[\panPercHi] = Pbind(
            \instrument, \playbufPerc,
            \dur, Prand([1/8, 1/4], inf),
            \buf, Prand(Forest.buf[\panPercHi], inf ),
            \db, Pwhite(-6, 0),
            \loop, 0,
            \attack, 0,
            \release, Pkey(\dur) * 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
        pbind[\panPercLid] = Pbind(
            \instrument, \playbufPerc,
            \dur, Prand([1/8, 1/4], inf),
            \buf, Prand(Forest.buf[\panPercLid], inf ),
            \db, Pwhite(-6, 0),
            \loop, 0,
            \attack, 0,
            \release, Pkey(\dur) * 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
        pbind[\kickTank] = Pbind(
            \instrument, \playbufPerc,
            \dur, Pseq([3/8, 3/8, 2/8], inf),
            \db, Pseq([0, -6, -12], inf),
            \buf, Prand( Forest.buf[\kickTank], inf ),
            \attack, 0,
            \release, 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
        pbind[\kickWoodshed] = Pbind(
            \instrument, \playbufPerc,
            \dur, Pseq([3/8, 3/8, 2/8], inf),
            \db, Pseq([0, -6, -12], inf),
            \buf, Prand( Forest.buf[\kickWoodshed], inf ),
            \attack, 0,
            \release, 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
        pbind[\kickWater] = Pbind(
            \instrument, \playbufPerc,
            \dur, Pseq([3/8, 3/8, 2/8], inf),
            \db, Pseq([0, -6, -12], inf),
            \buf, Prand( Forest.buf[\kickWater], inf ),
            \attack, 0,
            \release, 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
        pbind[\kickWoodenBridge] = Pbind(
            \instrument, \playbufPerc,
            \dur, Pseq([3/8, 3/8, 2/8], inf),
            \db, Pseq([0, -6, -12], inf),
            \buf, Prand( Forest.buf[\kickWoodenBridge], inf ),
            \attack, 0,
            \release, 2,
            \out, fxChain.in,
            \group, fxChain.group,
        );
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            var pfindur = Pfindur(
                dt.linlin(0.0, 1.0, 0.01, 3),
                pbind[kit],
                );

            fxChain.level = dt.linlin(0.0, 1.0, -6, 6).dbamp;

            if(quantize)
            {
                Pdef(pdef,
                    pfindur
                    )
            }{
                pfindur.play;
            }
        }
    }

    free{
        Pdef(pdef).clear;
        super.free;
    }
}
