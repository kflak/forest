ForestCeramics : ForestDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestCeramics;
    }

    initForestCeramics {
        this.prAddFx;
    }

    prAddFx {

        fxChain.addPar(
            \comb, [\mix, 0.2, \delay, 0.2, \decay, 1],
            \comb, [\mix, 0.2, \delay, 0.3, \decay, 1],
            \comb, [\mix, 0.2, \delay, 0.5, \decay, 1],
        );

        fxChain.add(\flanger,[
            \feedback, 0.12,
            \depth, 0.09,
            \rate, 0.03,
            \decay, 0.01,
            \mix, 0.3,
        ]);

        fxChain.add(\jpverb,[
            \revtime, 2,
            \mix, 0.2,
        ]);

        fxChain.add(\eq,[
            \hishelffreq, 400,
            \hishelfdb, -6,
            \mix, 1.0,
        ]);
    }
    
    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var duration = dt.linlin(0.0, 1.0, 1, 9);
            Pfindur(
                duration,
                Pbind(
                    \instrument, \playbuf,
                    \buf, Forest.buf[\ceramics].choose,
                    \bufframes, Pfunc{|ev| ev.buf.numFrames },
                    \buflength, Pfunc{|ev| ev.buf.duration },
                    \stretchfactor, Pwhite(0.5, 1.0),
                    \startPos, Pseg([0.0, Pkey(\bufframes) - 1], Pkey(\buflength) * rrand(0.2, 1)),
                    \rate, (-24..-12).midiratio.choose,
                    \pan, Pseg(Pwhite(-1.0, 1.0), Pkey(\buflength) * Pkey(\stretchfactor)),
                    \dur, Pwhite(0.05, 0.5),
                    \attack, Pkey(\dur) * Pwhite(0.5, 1.0),
                    \release, Pkey(\dur) * Pwhite(0.5, 1.0),
                    \legato, Pfunc{ Forest.mbData[id].delta.linlin(0.0, 1.0, 0.2, 4.0).clip(0.0, 1.5) },
                    \envelope, Pseg([-70, 0, 0, -70], [0.1, 0.5, 0.4] * duration),
                    \db, Pfunc{|ev|
                        var env = ev.envelope;
                        Forest.mbData[id].delta.linlin(0.0, 1.0, minAmp, maxAmp).clip(-70, 6) + env;
                    },
                    \group, fxChain.group,
                    \out, fxChain.in,
                )
            ).play;
        }
    }
}
