ForestBreathulator :  ForestDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeInTime=1, fadeOutTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestBreathulator;
    }

    initForestBreathulator {
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\eq, [
            \hishelfdb, 6,
            \hishelffreq, 400,
            \locut, 80,
            ]);
        fxChain.add(\jpverb, [
            \mix, 0.3,
            \revtime, Pwhite(1.0, 3.0),
            \lag, 2,
            ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(Forest.buf[\breath]),
                \dur, dt.linlin(0.0, 1.0, 1.0, 4.0),
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 2,  
                \rate, 1,
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 0.5),
                \grainfreq, Pwhite(2, 40),
                \grainsize, Pkey(\grainfreq).reciprocal * Pwhite(1.0, 4.0),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
