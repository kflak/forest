ForestAverageDeltas {
    var netAddr;
    var averageKenneth = 0;
    var averageKlli = 0;
    var sendRoutine;
    var prevDelta;
    var <>smoothingFactor = 0.9;
    var <>scalingFactor = 4;

    *new{
        ^super.new.initForestAverageDeltas;
    }

    initForestAverageDeltas {
        prevDelta = [0, 0];
        // netAddr = NetAddr.new("localhost", 57120);
        netAddr = NetAddr.new("25.1.107.255", 8000);
        this.prSendOut;
    }

    prSendOut {
        sendRoutine = fork{
            inf.do{
                // averageKlli = Forest.mbData[9].delta;
                // averageKenneth = Forest.mbData[13].delta;
                averageKlli = this.prSmooth(9, 0);
                averageKenneth = this.prSmooth(13, 1);
                // [averageKlli, averageKenneth].postln;
                netAddr.sendMsg('/avgDeltas', averageKlli, averageKenneth);
                0.2.wait;
            }
        }
    }

    prSmooth {|id, idx|
        var d_ = Forest.mbData[id].delta * scalingFactor;
        var d = d_ + (smoothingFactor * (prevDelta[idx] - d_));
        d = d.clip(0.0, 1.0);
        prevDelta[idx] = d;
        ^d;
    }

    free {
        sendRoutine.stop;
    }
}
