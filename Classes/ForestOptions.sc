ForestOptions {
    classvar <numInputBusChannels = 2;
    classvar <numSpeakers = 2;
    classvar <numSubs = 0;
    classvar <>numOutputBusChannels = 2;
    classvar <projectRoot;
    classvar <>latency = 0.1;

    *new {
        ^super.new.init;
    }

    init {
    }

    *numSpeakers_ {|val|
        numSpeakers = val;
        this.prCalcNumInputBusChannels;
    }

    prCalcNumInputBusChannels {|numSpeakers, numSubs|
        numInputBusChannels = numSpeakers + numSubs;
    }
}
