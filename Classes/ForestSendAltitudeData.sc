ForestSendAltitudeData {
    var netAddr;
    var altitudeKenneth = 0;
    var altitudeKlli = 0;
    var sendRoutine;

    *new{
        ^super.new.initForestSendAltitudeData;
    }

    initForestSendAltitudeData {
        // netAddr = NetAddr.new("localhost", 57120);
        netAddr = NetAddr.new("25.1.107.255", 8000);
        this.prSendOut;
    }

    prSendOut {
        sendRoutine = fork{
            inf.do{
                altitudeKlli = Forest.mbData[9].x;
                altitudeKenneth = Forest.mbData[13].x;
                netAddr.sendMsg('/altitudes', altitudeKlli, altitudeKenneth);
                0.1.wait;
            }
        }
    }

    free {
        sendRoutine.stop;
    }
}
