ForestWindy : ForestDeltaTrig {
    var <reverbTime = 3;
    var <reverbMix = 0.6;
    var <>minFreq = 80;
    var <>maxFreq = 20000;

    *new{|db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp= 0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestWindy;
    }

    initForestWindy{
        this.prAddFx;
    }

    reverbTime_ {|val| fxChain.fx[\jpverb].set(\revtime, val); reverbTime = val}
    reverbMix_ {|val| fxChain.fx[\jpverb].set(\mix, val); reverbMix = val}

    prAddFx {
        fxChain.add(\jpverb, [
            \revtime, reverbTime,
            \mix, reverbMix,
        ]);
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            var attack = dt.linlin(0.0, 1.0, 3, 2);
            var release = dt.linlin(0.0, 1.0, 1, 3);
            var length = attack + release;
            var db = dt.linlin(0.0, 1.0, minAmp, maxAmp);

            Pmono(
                \pink,
                \db, Pseg([-70, db, -70], [attack, release]),
                \dur, 0.1,
                \hicut, Pfunc{ Forest.mbData[id].x.linexp(0.0, 1.0, minFreq, maxFreq) },
                \out, fxChain.in,
                \group, fxChain.group,
                ).play();
        }
    }
}
