ForestBreatheInBreatheOut {
    var <out;
    var <fxChain;
    var <pbind;
    var <buffer;
    var <fadeInTime = 60;
    var <fadeOutTime = 5;
    var <db = 0;
    var <>stretchFactor = 2;

    *new{
        ^super.new.init;
    }

    init {
        out = Forest.mainBus;
        this.prCreateFxChain;
        this.prCreatePbind;
        this.play;
    }

    db_{|val| 
        fxChain.level = val.dbamp;
    }

    prCreateFxChain {
        fxChain = FxChain.new(
            level: db.dbamp,
            out: out,
            fadeInTime: fadeInTime,
            fadeOutTime: fadeOutTime,
            numInputBusChannels: 2, 
        );

        fxChain.add(\jpverb, [
            \revtime, 3,
            \lag, 5,
            \mix, Pwhite(0.1, 0.3), 5,
        ]);
    }

    prCreatePbind{
        buffer = Forest.buf[\breatheInBreatheOut][0];
        pbind = PbindProxy(
            \instrument, \grain,
            \buf, buffer,
            \numFrames, buffer.numFrames,
            \bufDur, buffer.duration,
            \jitter, 0,
            \startPos, Pn(
                Pfintime(
                    buffer.duration * stretchFactor,
                    Pseg([0, Pkey(\numFrames) - Pkey(\jitter) - 1], Pkey(\bufDur) * stretchFactor) + Pwhite(0, Pkey(\jitter)), 
                    ),
                inf
            ),
            \dur, 0.1,
            \attack, 0.1,
            \decay, 0.1,
            \db, Pseg([0, -26, 0], [40, 40], repeats: inf),
            \pan, Pseg([-1.0, 1.0, -1.0], [30, 30], repeats: inf),
            \out, fxChain.in,
            \group, fxChain.group,
        );
    }

    play {
        fxChain.play;
        pbind.play;
    }

    free{
        fork{
            fxChain.free;
            (fadeOutTime - 0.5).wait;
            pbind.clear;
        }
    }
}
