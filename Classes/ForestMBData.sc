ForestMBData {
    classvar <>resamplingFreq = 20;
    classvar <>oscOutAddress;
    classvar <>debug = false;

    var <>minibeeID;
    var <>windowSize;
    var buffer;
    var <delta, <x, <y, <z, <xdir, <ydir;
    var rawData, accelData, accelDataMul=15, accelDataOffset=7.0;
    var prevAccelData;
    var runningSum=0;
    var task;
    var oscFunc;
    var <>sendOscOut = false;
    var <>sendOscOutRaw = true;
    var <>server;
    var <xbus, <ybus, <zbus, <deltabus;
    var <altitude;
    var hasAltitude = false;
    var <minAltitude = 0;
    var <maxAltitude = 2;
    var rawAltitude = 0;
    var prevRawAltitude = 0;
    var <>smoothingFactor = 0.7;

    *new { arg minibeeID=10, windowSize=50;
        ^super.newCopyArgs(minibeeID, windowSize).init;
    }

    calibrateMinAltitude {
        if(hasAltitude){
            minAltitude = rawAltitude;
        }
    }

    calibrateMaxAltitude {
        if(hasAltitude){
            maxAltitude = rawAltitude;
        }
    }

    init {
        server = server ? Server.default;
        accelData = 0.0 ! 3;
        rawData = 0.0 ! 3;
        prevAccelData = 0.0 ! 3;
        oscOutAddress = NetAddr("localhost", 12345);
        buffer = List.newClear(windowSize).fill(0);
        xbus = Bus.control(server, 1);
        ybus = Bus.control(server, 1);
        zbus = Bus.control(server, 1);
        deltabus = Bus.control(server, 1);
        this.createOscFunc;
        this.createTask;
    }

    createOscFunc {
        oscFunc = OSCFunc({|oscdata|
            rawData = oscdata[2..];
            accelData = rawData[rawData.size - 3 ..];
            if(rawData.size == 9){
                hasAltitude = true;
                // rawAltitude = rawData[4];
                rawAltitude = rawData[4] + (smoothingFactor * (prevRawAltitude - rawData[4]));
                altitude = rawAltitude.linlin(minAltitude, maxAltitude, 0.0, 1.0);
                prevRawAltitude = rawAltitude;
            };
            accelData = accelData * accelDataMul - accelDataOffset;
            accelData = accelData.clip(0.0, 1.0);
            x = accelData[0];
            y = accelData[1];
            z = accelData[2];
        }, '/minibee/data', argTemplate: [minibeeID]);
        if(debug){
            [x, y, z].postln;
        }
    }

    sendOSC {
        if(sendOscOut){
            oscOutAddress.sendMsg("/minibee/data", minibeeID, x, y, z, delta);
        }
    }

    sendOSCRaw {
        if(sendOscOutRaw){
            oscOutAddress.sendMsg("/minibee/data", minibeeID, rawData[0], rawData[1], rawData[2]);
        }
    }

    createTask {
        task = TaskProxy.new({
            inf.do {
                this.calcDelta;
                this.sendOSC;
                this.sendOSCRaw;
                this.setBusses;
                resamplingFreq.reciprocal.wait;
            }
        }).play;
    }

    calcDelta {
        delta = (accelData - prevAccelData).abs.sum/3;
        prevAccelData = accelData.copy;
        ^delta;
    }

    setBusses {
        deltabus.set(delta);
        xbus.set(x);
        ybus.set(y);
        zbus.set(z);
    }
} 
