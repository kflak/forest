ForestMBDeltaTest : ForestDeltaTrig { 

    *new{|db=0, speedlim=0.5, threshold=0.05, minAmp= -18, maxAmp= -6, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestMBDeltaTest;
    }

    initForestMBDeltaTest {
        "init done".postln;
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\jpverb, [
            \revtime, 1,
            \mix, 0.4,
        ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            MBDeltaTrig.new(
                speedlim: 0.5,
                threshold: 0.05,
                minibeeID: id,
                minAmp: -18,
                maxAmp: -6,
                function: {|dt, minAmp, maxAmp|
                    Pbind(
                        \instrument, \default,
                        // \instrument, \sine,
                        \attack, 0,
                        \scale, Scale.minorPentatonic,
                        \octave, 3,
                        \degree, id - 9,
                        \dur, 1,
                        \release, 1,
                        \pan, Pwhite(-1.0, 1.0, 1),
                        \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                        \out, fxChain.in,
                        \group, fxChain.group,
                    ).play;
                };
            );
        }
    }
}
