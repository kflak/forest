ForestCoronaWhisperGrain : ForestDeltaTrig {

    *new{|db= -22, speedlim=0.5, threshold=0.05, minAmp= -12, maxAmp= -3, fadeInTime=1, fadeOutTime=30|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestCoronaWhisperGrain;
    }

    initForestCoronaWhisperGrain {
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\compressor, [
            \ratio, 4,
            \threshold, -12.dbamp,
            ]);
        fxChain.add(\eq, [
            \locut, 120,
            \hishelfdb, -6,
            \hishelffreq, 400,
            ]);
        fxChain.add(\jpverb, [
            \revtime, 2,
            \mix, 0.2,
            ]);
        fxChain.add(\greyhole, [
            \delayTime, 0.3,
            \feedback, 0.7,
            \mix, 0.2
            ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var startPos, currentFx, buf, numFrames,
            grainsize, numGrains, step, len, pos, rate, duration,
            attack, release, legato;
            buf = Forest.buf[\coronaWhisper].choose;
            numFrames = buf.numFrames;
            grainsize = 0.2;
            numGrains = 20;
            step = grainsize * Forest.server.sampleRate;
            ////in samples, not seconds....
            len = dt.linlin(0.0, 1.0, step, step * numGrains);
            startPos = 0;
            pos = (startPos, (startPos+step)..(startPos+len));
            pos = pos.select({|i| i < numFrames});
            rate = 1;
            attack = 0.2;
            release = 0.5;

            Pbind(
                \instrument, \playbuf,
                \buf, buf,
                \dur, grainsize,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * 2,
                \startPos, Pseq(pos),
                \legato, 1,
                \rate, Pwhite(0.99, 1.01),
                // \db, Pseg([-70, 0, -70], [attack, release]),
                \db, -6,
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }

}
