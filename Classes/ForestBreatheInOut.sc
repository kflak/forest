ForestBreatheInOut : ForestRandomSampleGranulate {

    *new{|db=0, speedlim=0.9, threshold=0.01, minAmp= -20, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestBreatheInOut;
    }

    initForestBreatheInOut {
        this.prAddFx;
        buf = Forest.buf[\breatheInBreatheOutChopped];
        timeStretch = 2;
    }

    prAddFx {
        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, Pwhite(0.1, 0.3), Pwhite(1, 5),
            ]); 
        fxChain.add(\eq, [
            \locut, 80,
            ]);
    }
}
