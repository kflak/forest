ForestLeavulator : ForestDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.05, minAmp= -6, maxAmp=6, fadeInTime=10, fadeOutTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestLeavulator;
    }

    initForestLeavulator {
        this.prAddFx;
    }

    prAddFx {
        
        fxChain.add(\comb, [
            \delay, Pwhite(0.002, 0.004),
            \decay, 1,
            \lag, 1,
            \mix, Pseg([0.0, 0.1, 0.0], [60, 60], repeats: inf),
            ]);
        fxChain.add(\eq, [
            \locut, 120
            ]);
        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, Pwhite(0.05, 0.3), Pwhite(1, 5),
            ]);
        fxChain.add(\greyhole, [
            \delayTime, 0.3,
            \feedback, 0.3,
            \mix, Pwhite(0.0, 0.3),
            ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buf = Forest.buf[\bushWalk];
            if (dt > 0.1) {
                buf = buf ++ Forest.buf[\walkGravel];
            };
            Pbind(
                \instrument, \playbuf,
                \buf, buf.choose,
                \dur, Pwhite(0.01, 0.1),
                \attack, Pkey(\dur),
                \release, Pkey(\dur),
                \startPos, Pfunc({|e| rrand(0, e.buf.numFrames - (e.dur * e.buf.sampleRate))}),
                \legato, 2,
                \rate, Pwhite(0.1, 2.0),
                \db, Pseg(
                    [-40, dt.linlin(0.0, 1.0, minAmp, maxAmp), -40],
                    [0.3, dt.linlin(0.0, 1.0, 1.0, 3.0)]
                ),
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }

}
