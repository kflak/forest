ForestWolfBreath : ForestDeltaTrig {
    var <>rateArray = #[1, -0.5, 0.25];

    *new{|db= 0, speedlim=2, threshold=0.03, minAmp= -20, maxAmp=0, fadeInTime=20, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestGrowly;
    }

    initForestGrowly{
        this.prAddFx;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(Forest.buf[\wolfBreath]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * Pwhite(1.0, 2.0),
                \rate, Pwrand(
                    rateArray, 
                    [1.0, 0.5, 0.25].normalizeSum,
                    inf
                ),
                \rateDev, Pwhite(0, 0.001),
                \posDev, Pwhite(0, 0.01),
                \playbackRate, Pwrand(
                    [-0.5, 0.25, 0.25],
                    [0.5, 0.25, 0.3].normalizeSum,
                    inf
                ),
                \grainsize, Pwhite(0.01, 0.8),
                \grainfreq, Pwhite(4, 20),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }

    prAddFx {
        fork{
            Forest.server.sync;
            fxChain.add(\eq,[
                \locut, 80,
                \mix, 1.0,
                ]);

            fxChain.add(\jpverb,[
                \revtime, 3,
                \mix, 0.3,
                ]);

            fxChain.add(\flanger,[
                \feedback, 0.08,
                \depth, 0.04,
                \rate, 0.03,
                \decay, 0.01,
                \mix, 0.3,
                ]);

            fxChain.add(\compressor, [
                \ratio, 4,
                \threshold, -6.dbamp,
                ]);
        }
    }
}
