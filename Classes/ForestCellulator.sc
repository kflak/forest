ForestCellulator : ForestDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.07, minAmp= -6, maxAmp=0, fadeInTime=10, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestCellulator;
    }

    initForestCellulator {
        this.prAddFx;
    }

    prAddFx {
        fxChain.addPar(
            \comb, [\mix, 0.2, \delay, 0.2, \decay, 1, \amp, 1/3],
            \comb, [\mix, 0.2, \delay, 0.5, \decay, 1, \amp, 1/3],
            \comb, [\mix, 0.2, \delay, 0.7, \decay, 1, \amp, 1/3],
        );
        fxChain.add(\jpverb, [
            \revtime, 2,
            \lag, 1,
            \mix, Pwhite(0.0, 0.2),
        ]);
        fxChain.add(\greyhole, [
            \delayTime, 0.2,
            \feedback, 0.5,
            \lag, 1,
            \mix, Pwhite(0.0, 0.2),
        ]);
        fxChain.add(\eq, [
            \locut, 300,
            \hishelfdb, -12,
        ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(Forest.buf[\celloLublin]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 3),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,
                \rate, Prand([0.25, 0.5], inf),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 20),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}

