ForestRandomSampleGranulate : ForestDeltaTrig {
    classvar buf_;

    var <>buf;
    var <>loop = false;
    var <>overlap = 2;
    var <>triggerFreq = 10;
    var <>grainSize = 0.1;
    var <>legato = 2;
    var <>releaseMul = 4;
    var <>minRate = 1.0;
    var <>maxRate = 1.0;
    var <>minPan = -1.0;
    var <>maxPan = 1.0;
    var <>timeStretch = 1.0;
    var <>pitchShiftRange = #[1, 1];
    var <>jitter = 0;
    var <>minAttack = 0.1;
    var <>maxAttack = 2;
    var <>minRelease = 0.1;
    var <>maxRelease = 2;
    var <reverbTime = 3;
    var <reverbMix = 0.4;
    var <flangerMix = 0.3;

    *new{|db= -6, speedlim=0.5, threshold=0.1, minAmp= -30, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestRandomSampleGranulate;
    }

    initForestRandomSampleGranulate{
        this.prAddFx;
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            var buffer = buf.choose;
            var ampScale = dt.linlin(0.0, 1.0, minAmp, maxAmp);
            var attack = dt.linlin(0.0, 1.0, minAttack, maxAttack);
            var release = dt.linlin(0.0, 1.0, minRelease, maxRelease);
            freeMBsBufferTime = attack + release;

            Pbind(
                \instrument, \grain,
                \buf, buffer,
                \dur, triggerFreq.reciprocal,
                \startPos, Pseg([0, buffer.numFrames], buffer.duration * timeStretch) + Pwhite(0, jitter),
                \legato, overlap,
                \attack, Pkey(\dur) * overlap,
                \release, Pkey(\dur) * overlap, 
                \loop, false,
                \rate, Pwhite(pitchShiftRange[0], pitchShiftRange[1]).midiratio,
                \pan, Pwhite(minPan, maxPan),
                \db, Pseg([-70, 0, -70], [attack, release]),
                \out, fxChain.in,
                \group, fxChain.group,
                ).play;
        }

    }

    reverbTime_ {|val| fxChain.fx[\jpverb].set(\revtime, val); reverbTime = val}
    reverbMix_ {|val| fxChain.fx[\jpverb].set(\mix, val); reverbMix = val}
    flangerMix_ {|val| fxChain.fx[\flanger].set(\mix, val); flangerMix = val}

    prAddFx {
        fork{
            Forest.server.sync;
            fxChain.add(\jpverb,[
                \revtime, reverbTime,
                \mix, reverbMix,
                ]);

            fxChain.add(\flanger,[
                \feedback, 0.08,
                \depth, 0.04,
                \rate, 0.03,
                \decay, 0.01,
                \mix, flangerMix,
                ]);
        }
    }
}
