ForestFocus : ForestDeltaTrig {

    *new{|db= -6, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestFocus;
    }

    initForestFocus {
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\compressor, [
            \ratio, 4,
            \threshold, -12.dbamp,
            ]);
        fxChain.addPar(
            \comb, [\mix, 0.2, \delay, 0.2, \decay, 4, \amp, 1/3],
            \comb, [\mix, 0.2, \delay, 0.5, \decay, 4, \amp, 1/3],
            \comb, [\mix, 0.2, \delay, 0.7, \decay, 4, \amp, 1/3],
            );
        fxChain.add(\jpverb, [
            \revtime, 2,
            \mix, 0.2,
            ]);
        fxChain.add(\eq, [
            \locut, 660,
            ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buf;
            if (Forest.language == \Estonian){
                buf = Forest.buf[\focus]++Forest.buf[\kullipoem_ee_chopped]++Forest.buf[\kullipoem_en_chopped];
            }{
                buf = Forest.buf[\focus]++Forest.buf[\kullipoem_en_chopped];
            };

            Pbind(
                \instrument, \playbuf,
                \buf, Prand(buf),
                \dur, Pfunc({|e| e.buf.duration }),
                \attack, Pkey(\dur) * 0.1,
                \release, Pkey(\dur) * 0.3,
                \legato, 0.9,
                \db, -6,
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
