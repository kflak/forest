ForestBd : ForestDeltaTrig{
    *new{|db= -6, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestBd;
    }

    initForestBd {
        this.prAddFx;
    }

    prAddFx {

        fxChain.add(\eq, [
            \locut, 60, 
            \hicut, 200,
        ]);
        fxChain.add(\jpverb, [
            \mix, 0.5,
            \revtime, 3, 
        ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \playbuf,
                \buf, Prand(Forest.buf[\bd]),
                \dur, 1,
                \release, Pkey(\dur) * 2,  
                \loop, 0,
                \legato, 0.1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
