ForestTrumpetGrainDelay{
    var <in, <out, <minAmp, <maxAmp, <threshold, <monitorOut, <monitorLevel;
    var <fxChain;
    var <monitorFxChain;
    var <grainDelay;
    var <fadeInTime = 1;
    var <fadeOutTime = 60;

    *new{|in=2, out=0, minAmp=0, maxAmp=1, threshold=0.05, monitorOut=0, monitorLevel=1|
        ^super.newCopyArgs(in, out, minAmp, maxAmp, threshold, monitorOut, monitorLevel).init;
    }
    
    init{
        this.prCreateFxChain;
        this.prCreateMonitorFxChain;
        this.prCreateGrainDelay;
    }

    prCreateGrainDelay{
        grainDelay = ForestGrainDelay.new(
            in: in,
            // out: Forest.mainBus,
            out: fxChain.in,
            group: fxChain.group,
            monitorOut: monitorFxChain.in,
            minAmp: minAmp,
            maxAmp: maxAmp,
            threshold: threshold
        )
    }

    prCreateMonitorFxChain{
        monitorFxChain = FxChain.new(
            level: -6.dbamp,
            out: out,
            fadeInTime: fadeInTime,
            fadeOutTime: fadeOutTime,
        );
        monitorFxChain.add(\jpverb,[
            \revtime, Pwhite(1.0, 4.0),
            \mix, Pwhite(0.3, 0.4),
        ]);
    }

    prCreateFxChain{
        fxChain = FxChain.new(
            level: -6.dbamp,
            out: out,
            fadeInTime: fadeInTime,
            fadeOutTime: fadeOutTime,
        );
        fxChain.add(\flanger, [
            \feedback, 0.08,
            \depth, 0.04,
            \rate, 0.03,
            \decay, 0.01,
            \mix, 0.3,
        ]);
        fxChain.add(\jpverb,[
            \revtime, Pwhite(1.0, 4.0),
            \mix, Pwhite(0.1, 0.4),
        ]);
    }

    play {
        fork{
            1.wait;
            fxChain.play;
            monitorFxChain.play;
            grainDelay.play([9, 10, 11, 12]);
        }
    }

    free {
        fork{
            grainDelay.free;
            5.wait;
            fxChain.free;
            monitorFxChain.free;
        }
    }
}
