ForestKlang : ForestDeltaTrig {
    var <>octave = #[4, 5];
    var <>degrees = #[0];
    var <>dur = 0.25;
    var <>attack = 0;

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -50, maxAmp= -25, fadeInTime=60, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestKlang;
    }

    initForestKlang{
        this.prAddFx;
    }

    prAddFx{

        fxChain.add(\eq,[
            \hicut, Pseg(Pexprand(800, 2000), Pwhite(1, 5)),
            \locut, Pseg(Pexprand(80, 300), Pwhite(1, 5)),
            ]);

        fxChain.add(\lfnoise, [
            \freq, Pseg(Pwhite(1, 10), Pwhite(0.5, 1)),
            \mix, 1.0,
            ]);

        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, 0.4,
            ]);
    }

    mbDeltaTrigFunction{
        ^{|dt, minAmp, maxAmp, id|
            var atk = dt.linlin(0.0, 1.0, 1, 2);
            var release = atk;
            var amp = dt.linlin(0.0, 1.0, minAmp, maxAmp);
            var deg = degrees.choose;
            Pbind(
                \instrument, \klang,
                \dur, dur,
                \attack, attack,
                \release, Pkey(\dur) * 2,
                \root, 6,
                \octave, octave,
                \degree, [0, 5, 7] + deg,
                \spread, Pwhite(1.99, 2.01),
                \legato, 0.8,
                \pan, Pwhite(-1.0, 1.0),
                \db, Pseg([-60, amp, -60], [atk, release]),
                \out, fxChain.in, 
                \group, fxChain.group,
            ).play;
        }
    }
}
