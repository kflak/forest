// Abstract superclass for all mbDeltaTrigs. Defines an
// mbDeltaTrig and an fxChain that subclasses can add to.
// mbDeltatrigs need to define an mbFunction, fxChains need to 
// add the required fx. 
// the fxChain is routed to an eq synth with setter for the various 
// params
// BBSMBDeltaTrig implements play and free functions.
ForestDeltaTrig {
    classvar <>emulateMBs = false;

    var <db;
    var <speedlim;
    var <threshold;
    var <minAmp;
    var <maxAmp;
    var <fadeInTime;
    var <fadeOutTime; // deliberately long, so as to ensure all resources are freed before end
    var <fxChain;
    var <mbIDs;
    var <mbDeltaTrigs;
    var <eq;
    var <eqbus;
    var <locut = 20;
    var <hicut = 20000;
    var <hishelfdb = 0;
    var <hishelffreq = 1500;
    var <loshelfdb = 0;
    var <loshelffreq = 200;
    var <peakdb = 0;
    var <peakrq = 1;
    var <peakfreq = 600;
    var <>freeMBsImmediately = true;
    var <>freeMBsBufferTime = 1;

    *new{|db=0, speedlim=0.5, threshold=0.1, minAmp= -40, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.newCopyArgs(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).init;
    }

    init {
        eqbus = Bus.audio(Forest.server, Forest.numSpeakers);
        this.prCreateFxChain;
        this.prCreateEq;
        mbIDs = Forest.mb;
        this.prCreateMBDeltaTrigs;
    }

    db_ {|val| fxChain.level = val.dbamp; db = val}
    speedlim_{|val| mbDeltaTrigs.do(_.speedlim_(val)); speedlim = val}
    threshold_{|val| mbDeltaTrigs.do(_.threshold_(val)); threshold = val}
    minAmp_{|val| mbDeltaTrigs.do(_.minAmp_(val)); minAmp = val}
    maxAmp_{|val| mbDeltaTrigs.do(_.maxAmp_(val)); maxAmp = val}
    fadeInTime_{|val| fxChain.fadeInTime = val; fadeInTime = val}
    fadeOutTime_{|val| fxChain.fadeOutTime = val; fadeOutTime = val}
    locut_ {|val| eq.set(\locut, val); locut = val}
    hicut_ {|val| eq.set(\hicut, val); hicut = val }
    hishelfdb_ {|val| eq.set(\hishelfdb, val); hishelfdb = val }
    hishelffreq_ {|val| eq.set(\hishelffreq, val); hishelffreq = val}
    loshelfdb_ {|val| eq.set(\loshelfdb, val); loshelfdb = val}
    loshelffreq_ {|val| eq.set(\loshelffreq, val); loshelffreq = val}
    peakdb_ {|val| eq.set(\peakdb, val); peakdb = val}
    peakrq_ {|val| eq.set(\peakrq, val); peakrq = val}
    peakfreq_ {|val| eq.set(\peakfreq, val); peakfreq = val}

    prCreateFxChain{
        fxChain = FxChain.new(
            level: db.dbamp,
            out: eqbus,
            fadeInTime: fadeInTime,
            fadeOutTime: fadeOutTime,
            );
    }

    prCreateEq {
        eq = Synth(\eq, [
            \in, eqbus,
            \out, Forest.mainBus,
            \locut, locut,
            \hicut, hicut,
            \hishelfdb, hishelfdb,
            \hishelffreq, hishelffreq,
            \loshelfdb, loshelfdb,
            \loshelffreq, loshelffreq,
            \peakdb, peakdb,
            \peakrq, peakrq,
            \peakfreq, peakfreq
            ], target: fxChain.group, addAction: \addToTail)
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            "Triggered from superclass".warn;
            [id, dt, minAmp, maxAmp].postln;
        };
    }

    prCreateMBDeltaTrigs{
        mbDeltaTrigs = mbIDs.collect{|id, idx|
            ForestMBDeltaTrig.new(
                speedlim: speedlim, 
                threshold: threshold,
                minibeeID: id,
                minAmp: minAmp,
                maxAmp: maxAmp,
                function: this.mbDeltaTrigFunction;
            );
        }
    }
    play {|...args|
        // TODO: implement this once FxChain is rock solid
        // var c = Condition.new;
        // var t;
        fork{
            var mbs;

            if(args.isEmpty, {
                mbs = Forest.mb;
            },{
                mbs = args;
            });

            fxChain.play;

            // c.wait;
            1.wait;
            
            mbs.do{|id| 
                var index = mbIDs.indexOf(id);
                mbDeltaTrigs[index].play;
            };
        };

        // t = Task({
        //     var isPlaying = fxChain.fx[\fxOut].isPlaying;
        //     var count = 0;
        //     loop{
        //         0.1.wait;
        //         c.test = isPlaying;
        //         c.signal;
        //         if(isPlaying){ t.stop };
        //         ("Attempt number: " ++ count).postln;
        //         count = count + 1;
        //     }
        // }).start;
    }

    freeMB {|mb|
        var index = mbIDs.indexOf(mb);
        mbDeltaTrigs[index].free;
    }

    free {
        fork{
            if (freeMBsImmediately){
                mbDeltaTrigs.do(_.free);
            }{
                SystemClock.sched(
                    max(fadeOutTime - freeMBsBufferTime, 0),
                    {
                        mbDeltaTrigs.do(_.free);
                    })
            };
            fxChain.free;
            fxChain.fadeOutTime.wait;
            eq.release(1);
            1.wait;
            eqbus.free;
        }
    }

}
