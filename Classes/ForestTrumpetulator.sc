ForestTrumpetulator : ForestDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.035, minAmp= -12, maxAmp=6, fadeInTime=10, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestTrumpetulator;
    }

    initForestTrumpetulator {
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\eq, [
            \hishelfdb, -12,
            \hishelffreq, 500,
            \locut, 250
        ]); 
        fxChain.add(\flanger, [
            \feedback, 0.08,
            \depth, 0.04,
            \rate, 0.03,
            \decay, 0.01,
            \mix, 0.2, 
        ]); 
        fxChain.add(\jpverb, [
            \mix, 0.2,
            \revtime, 3
        ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand((Forest.buf[\brpiano]++Forest.buf[\trumpet])),
                \dur, dt.linlin(0.0, 1.0, 0.5, 1),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,  
                \rate, Prand([0.25, 1], inf),
                \rateDev, Pwhite(0.0, 0.15),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.001, 0.15),
                \grainfreq, Pwhite(1, 12),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
