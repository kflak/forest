ForestStormulator : ForestDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.035, minAmp= -12, maxAmp=6, fadeInTime=10, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestStormulator;
    }

    initForestStormulator {
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\eq, [
            \hishelffreq, 400,
            \hishelfdb, -3,
            \locut, 120,
        ]);
        fxChain.add( \compressor, [
            \thresh, -12.dbamp,
            \ratio, 8,
            \amp, 0.dbamp,
        ]); 
        fxChain.add(\limiter, [
            \limit, -3.dbamp,
        ]);
        fxChain.add(\greyhole, [
            \feedback, 0.6,
            \delayTime, 0.3,
            \mix, 0.3,
        ]);
        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, 0.2,
        ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor, 
                \buf, Prand(Forest.buf[\coronaWhisper]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur),
                \release, Pkey(\dur),  
                \rate, Pwhite(0.5, 1),
                \rateDev, Pwhite(0.0, 0.05),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 1),
                \grainsize, Pwhite(0.03, 0.01),
                \grainfreq, Pkey(\grainsize).reciprocal,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}

