ForestGrowly : ForestDeltaTrig {
    var <pmono;
    var <minAttack = 2;
    var <maxAttack = 0.1;
    var <minRelease = 0.2;
    var <maxRelease = 1;

    *new{|db= -6, speedlim=2, threshold=0.03, minAmp= -20, maxAmp=0, fadeInTime=20, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestGrowly;
    }

    initForestGrowly{
        this.prAddFx;
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var pmindex = rrand(0.5, 1.5);
            var attack = dt.linlin(0.0, 1.0, minAttack, maxAttack);
            var release = dt.linlin(0.0, 1.0, minRelease, maxRelease);

            pmono = Pmono(
                \pmFB,
                \dur, 0.1,
                \db, Pseg([-70, 0, -70], [attack, release]),
                \lag, Pkey(\dur),
                \freq, Pfunc{
                    var freqMul = rrand(1.0, 1.5);
                    var minFreq = rrand(40, 80);
                    var maxFreq = minFreq * freqMul;
                    var x = Forest.mbData[id].x;
                    var freq = x.linexp(0.0, 1.0, minFreq, maxFreq);
                    freq
                },
                \modfreq, Pfunc{|ev| ev.use{ ~freq.() * rrand(0.4, 0.6)}},
                \modfeedback, Pfunc{ Forest.mbData[id].y * 2 },
                \pmindex, Pfunc{
                    Forest.mbData[id].x * pmindex
                },
                \out, fxChain.in,
                \group, fxChain.group,
                ).play;
        }
    }

    prAddFx {
        fork{
            Forest.server.sync;
            fxChain.add(\eq,[
                \locut, 80,
                \mix, 1.0,
                ]);

            fxChain.add(\jpverb,[
                \revtime, 2,
                \mix, 0.1,
                ]);

            fxChain.add(\flanger,[
                \feedback, 0.08,
                \depth, 0.04,
                \rate, 0.03,
                \decay, 0.01,
                \mix, 0.3,
                ]);

            fxChain.add(\compressor, [
                \ratio, 4,
                \threshold, -6.dbamp,
                ]);
        }
    }
}
