ForestRandomSample : ForestDeltaTrig {

    var <>buf;
    var <>loop = false;
    var <>minDur = 0.1;
    var <>maxDur = 2.0;
    var <>minAttack = 0.1;
    var <>maxAttack = 1;
    var <>minRelease = 0.1;
    var <>maxRelease = 1;
    var <>repeats = 1;
    var <>legato = 2;
    var <>releaseMul = 4;
    var <>minRate = 1.0;
    var <>maxRate = 1.0;
    var <>minPan = -1.0;
    var <>maxPan = 1.0;

    *new{|db=0, speedlim=0.3, threshold=0.01, minAmp= -40, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestRandomSample;
    }

    initForestRandomSample{}

    mbDeltaTrigFunction{
        // must return a function for the MBDeltaTrig
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \playbuf,
                \buf, buf.choose,
                \dur, Pfunc{|ev| ev.buf.duration.clip(minDur, maxDur)},
                \loop, loop,
                \attack, dt.linlin(0.0, 1.0, minAttack, maxAttack),
                \release, dt.linlin(0.0, 1.0, minRelease, maxRelease),
                \pan, Pwhite(minPan, maxPan, repeats),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \out, fxChain.in,
                \group, fxChain.group,
                ).play;
        }
    }
}
