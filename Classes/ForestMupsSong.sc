ForestMupsSong : ForestShuffle {

    *new{|db= -24, speedlim=0.3, threshold=0.01, minAmp= -6, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestMupsSong;
    }

    initForestMupsSong {
        this.prAddFx;
        buf = Forest.buf[\mupssongFull];
        grainSize = 0.2;
        minRate = 0.99;
        maxRate = 1.01;
        freeMBsImmediately = false;
    }

    prAddFx {
        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, Pwhite(0.01, 0.3), Pwhite(1, 5),
            ]); 
        fxChain.add(\greyhole, [
            \delayTime, 0.3,
            \feedback, 0.9,
            \mix, Pwhite(0.01, 0.3), Pwhite(1, 5),
            ]); 
        fxChain.add(\eq, [
            \locut, 80,
            ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            var buffer = buf.choose;
            var numFrames = buffer.numFrames;
            var step = grainSize * Forest.server.sampleRate;
            var totalDuration = dt.linlin(0.0, 1.0, step, step * 5);
            var idx = Forest.mb.indexOf(id);
            var pos = (currentPos[idx], currentPos[idx]+step..currentPos[idx]+totalDuration); 

            if(loop, 
            {
                currentPos[idx] = pos[pos.size-1].mod(numFrames); 
            },
            {
                currentPos[idx] = pos[pos.size-1]; 
            });

            Pbind(
                \instrument, \playbuf,
                \buf, buffer,
                \dur, grainSize,
                \attack, Pkey(\dur),
                \release, Pkey(\dur) * releaseMul,
                \startPos, Pseq(pos),
                \legato, legato,
                \loop, 0,
                \rate, Pfunc{ Forest.mbData[id].x.linexp(0.0, 1.0, 0.25, 2.0) },
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(minPan, maxPan),
                \out, fxChain.in,
                \group, fxChain.group,
                ).play;
        };
    }}
