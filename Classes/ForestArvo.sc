ForestArvo : ForestDeltaTrig {
    var <>rateArray = #[1, -0.5, 0.25];
    var <distMix = 0.0;
    var <reverbMix = 0.4;
    
    *new{|db=0, speedlim=0.5, threshold=0.03, minAmp= -20, maxAmp=0, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestArvo;
    }

    initForestArvo{
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\noiseMod, [
            \modfreq, Pwhite(400, 800), Pwhite(2, 4),
            \lag, 3,
            \depth, 4,
            \mix, distMix,
        ]);
        fxChain.add(\jpverb, [
            \lag, 5,
            \revtime, 3,
            \mix, reverbMix,
        ]);
        fxChain.add(\eq, [
            \locut, 120,
            \hishelffreq, 400,
            \hishelfdb, -6
        ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(Forest.buf[\arvo]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * Pwhite(1.0, 2.0),
                \rate, Pwrand(
                    rateArray, 
                    [1.0, 0.5, 0.25].normalizeSum,
                    inf
                ),
                \rateDev, Pwhite(0, 0.001),
                \posDev, Pwhite(0, 0.01),
                \playbackRate, Pwrand(
                    [-0.5, 0.25, 0.25],
                    [0.5, 0.25, 0.3].normalizeSum,
                    inf
                ),
                \grainsize, Pwhite(0.01, 0.8),
                \grainfreq, Pwhite(4, 20),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }

    distMix_{|val| fxChain.fx[\noiseMod].set(\mix, val); distMix = val}
    reverbMix_{|val| fxChain.fx[\jpverb].set(\mix, val); reverbMix = val}
}
