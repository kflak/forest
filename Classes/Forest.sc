Forest {
    classvar <>numInputBusChannels = 8;
    classvar <numSpeakers = 2;
    classvar <numSubs = 0;
    classvar <numOutputBusChannels = 2;
    classvar <packageRoot;
    classvar <server, options;
    classvar <mbData;
    classvar <mb = #[9, 10, 11, 12, 13, 14, 15, 16, 17, 19];
    classvar <buf;
    classvar <mainLevel = 1.0;
    classvar <subLevel = 0.316;
    classvar <mainGroup, <mainBus, <main, <sub;
    classvar <>language = \Estonian;

    *new {
        ^super.new.init;
    }

    *initClass{
        packageRoot = Forest.filenameSymbol.asString.dirname.dirname;
    }

    init {

        mbData = IdentityDictionary.new;
        // ForestMBData.debug = true;
        mb.do{|id| mbData.put(id, ForestMBData.new(id))};
        ForestMBDeltaTrig.mbData = mbData;

        server = server ?? {Server.default};
        options = server.options;
        options.numInputBusChannels = numInputBusChannels;
        options.numOutputBusChannels = numOutputBusChannels;
        FxChain.numSpeakers = numSpeakers;
        server.latency = 0.1;
        server.waitForBoot({
            load(packageRoot+/+"SynthDefs.scd");

            server.sync;

            mainGroup = Group.new();
            mainBus = Bus.audio(server, numSpeakers);
            main = Synth(\route, [
                \in, mainBus, 
                \amp, mainLevel,
                \out, 0
                ], mainGroup);
            if(numSubs > 0){
                sub = Synth(\mono, [
                    \in, mainBus, 
                    \amp, subLevel, 
                    \out, numSpeakers
                    ], mainGroup);
            };

            server.sync;

            this.prCheckLangPort;
            Forest.loadBuffers(packageRoot+/+"audio");
        });
    }

    *numSpeakers_ {|val|
        numSpeakers = val;
        this.prSetNumOutputBusChannels;
    }

    *numSubs_ {|val|
        numSubs = val;
        this.prSetNumOutputBusChannels;
    }

    *prSetNumOutputBusChannels {
        numOutputBusChannels = numSpeakers + numSubs;
    }

    *mainLevel_ {|level|
        mainLevel = level;
        main.set(\amp, mainLevel);
    }

    *subLevel_ {|level|
        subLevel = level;
        sub.set(\amp, subLevel);
    }

    *loadBuffers {|path|
        server.doWhenBooted({
            var audioDirCount=0;
            buf = Dictionary.new;
            PathName((path).standardizePath).folders.do({|folder|
                buf[folder.folderName.asSymbol] = SoundFile.collectIntoBuffersMono(folder.fullPath+/+"*");
                folder.fullPath.postln;
                audioDirCount = audioDirCount + 1; 
            });
        "% audio directories loaded\n".postf(audioDirCount);
        });
    }

    cuePlayer {|cuePlayer|
        OSCdef(\cue, {
            {cuePlayer.next}.defer;
        }, "/1/cue");
    }

    prCheckLangPort {
        if (NetAddr.langPort != 57120) {
            "******************************".postln;
            "******************************".postln;
            "************ WARNING!*********".postln;
            "*** LangPort is not 57120! ***".postln;
            "*** Check for other running***".postln;
            "*** instances of sclang!   ***".postln;
            "******************************".postln;
            "******************************".postln;
            "******************************".postln;
        };
    }
}
