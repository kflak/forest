ForestTreePerculator : ForestDeltaTrig {

    *new{|db= -6, speedlim=0.5, threshold=0.1, minAmp= -6, maxAmp=0, fadeInTime=20, fadeOutTime=10|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestTreePerculator;
    }

    initForestTreePerculator {
        this.prAddFx;
    }

    prAddFx {

        fxChain.add(\eq, [
            \locut, 60, 
            ]);
        fxChain.add(\jpverb, [
            \mix, 0.3,
            \revtime, 2, 
            ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \playbuf,
                \buf, Prand(Forest.buf[\treeperc], inf),
                \rate, 2,
                \dur, Pseq((0.125!4)),
                \release, Pkey(\dur) * 2,  
                \legato, 0.1,
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
            ).play;
        }
    }
}
