ForestByssanlull : ForestShuffle {

    *new{|db= -42, speedlim=0.3, threshold=0.01, minAmp= -6, maxAmp=0, fadeInTime=1, fadeOutTime=60|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestByssanlull;
    }

    initForestByssanlull {
        this.prAddFx;
        buf = Forest.buf[\byssanlull];
        grainSize = 0.4;
        freeMBsImmediately = false;
        loop = true;
    }

    prAddFx {

        fxChain.add(\jpverb, [
            \revtime, Pwhite(0.1, 4), Pwhite(1, 5),
            \mix, 0.3,
            ]); 
        fxChain.add(\eq, [
            \locut, 80,
            \hicut, 1600,
            ]);
    }
}
