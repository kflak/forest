ForestMeilAiaSong {
    var <out;
    var <fxChain;
    var <pbind;
    var <buffer;
    var <fadeInTime = 60;
    var <fadeOutTime = 40;
    var <db = 0;

    *new{
        ^super.new.init;
    }

    init {
        out = Forest.mainBus;
        this.prCreateFxChain;
        this.prCreatePbind;
        this.play;
    }

    db_{|val| 
        fxChain.level = val.dbamp;
    }

    prCreateFxChain {
        fxChain = FxChain.new(
            level: db.dbamp,
            out: out,
            fadeInTime: fadeInTime,
            fadeOutTime: fadeOutTime,
            numInputBusChannels: 2, 
        );

        fxChain.add(\jpverb, [
            \revtime, 3,
            \mix, 0.4
        ]);
    }

    prCreatePbind{
        buffer = Forest.buf[\meilaja][0];
        pbind = PbindProxy(
            \instrument, \grain,
            \buf, buffer,
            \numFrames, buffer.numFrames,
            \bufDur, buffer.duration,
            \jitter, 1000,
            \startPos, Pn(
                Pfintime(
                    buffer.duration,
                    Pseg([0, Pkey(\numFrames) - Pkey(\jitter) - 1], Pkey(\bufDur)) + Pwhite(0, Pkey(\jitter)), 
                    ),
                inf
            ),
            \dur, 0.1,
            \attack, 0.1,
            \decay, 0.1,
            \db, Pseg([0, 0, -26, -6, -70], [15, 20, 20, 20]),
            \pan, Pseg([-1.0, 1.0, -1.0], [30, 30], repeats: inf),
            // \db, 0,
            \out, fxChain.in,
            \group, fxChain.group,
        );
    }

    play {
        fxChain.play;
        pbind.play;
    }

    free{
        fork{
            fxChain.free;
            (fadeOutTime - 0.5).wait;
            pbind.clear;
        }
    }
}
