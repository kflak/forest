ForestEarthulator : ForestDeltaTrig {

    *new{|db=0, speedlim=0.5, threshold=0.07, minAmp= -9, maxAmp=3, fadeInTime=1, fadeOutTime=20|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp, fadeInTime, fadeOutTime).initForestEarthulator;
    }

    initForestEarthulator{
        this.addFx;
    }

    addFx {
        fxChain.add( \compressor,[
            \thresh, 4.dbamp,
            \ratio, 8,
            \amp, 9.dbamp,
            ]);
    }

    mbDeltaTrigFunction {
        ^{|dt, minAmp, maxAmp, id|
            Pbind(
                \instrument, \grbufphasor,
                \buf, Prand(Forest.buf[\dancefloor]),
                \dur, dt.linlin(0.0, 1.0, 0.5, 2),
                \attack, Pkey(\dur) * 0.2,
                \release, Pkey(\dur) * 2,  
                \rate, Prand([0.25, 1], inf),
                \rateDev, Pwhite(0.0, 0.5),
                \posDev, Pwhite(0.0, 0.3),
                \playbackRate, Pwhite(0.1, 2),
                \grainsize, Pwhite(0.01, 0.5),
                \grainfreq, Pwhite(2, 40),
                \db, dt.linlin(0.0, 1.0, minAmp, maxAmp),
                \pan, Pwhite(-1.0, 1.0, 1),
                \panDev, Pwhite(0.0, 1.0),
                \out, fxChain.in,
                \group, fxChain.group,
                ).play;
        }
    }
}
