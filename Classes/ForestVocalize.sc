ForestVocalize : ForestRandomSample {
    var <out;
    var <threshold;
    var <group;
    var <buf;
    var <fxChain;
    var <>repeats = 3;
    var <mbRandomSample;
    var <db = -6;

    *new{|db=0, speedlim=0.3, threshold=0.1, minAmp= -40, maxAmp=0|
        ^super.new(db, speedlim, threshold, minAmp, maxAmp).initForestVocalize;
    }

    initForestVocalize{
        buf = Forest.buf[\vocality];
        this.prAddFx;
    }

    prAddFx {
        fxChain.add(\flanger, [
            \feedback, 0.08,
            \depth, 0.04,
            \rate, 0.03,
            \decay, 0.01,
            \mix, 0.3,
            ]);
        fxChain.add(\jpverb,[
            \revtime, Pwhite(1.0, 4.0),
            \mix, Pwhite(0.2, 0.7),
            ]);
    }
}
