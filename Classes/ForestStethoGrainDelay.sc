ForestStethoGrainDelay{
    var <in, <out, <minAmp, <maxAmp, <threshold, <monitorLevel;
    var <fxChain;
    var <grainDelay;

    *new{|in=2, out=0, minAmp=0, maxAmp=1, threshold=0.05, monitorOut=0, monitorLevel=1|
        ^super.newCopyArgs(in, out, minAmp, maxAmp, threshold).init;
    }
    
    init{
        this.prCreateFxChain;
        this.prCreateGrainDelay;
    }

    prCreateGrainDelay{
        grainDelay = ForestGrainDelay.new(
            in: in,
            out: fxChain.in,
            group: fxChain.group,
            monitorOut: fxChain.in,
            minAmp: minAmp,
            maxAmp: maxAmp,
            threshold: threshold
        )
    }

    prCreateFxChain{
        fxChain = FxChain.new(
            level: -6.dbamp,
            out: out,
            fadeInTime: 1,
            fadeOutTime: 1,
        );
        fxChain.add(\jpverb,[
            \revtime, Pwhite(1.0, 4.0),
            \mix, Pwhite(0.1, 0.4),
        ]);
        fxChain.add(\flanger, [
            \feedback, 0.08,
            \depth, 0.04,
            \rate, 0.03,
            \decay, 0.01,
            \mix, 0.3,
        ]);
        fxChain.add(\dopplerPitchShift, [
            \ratio, -12.midiratio,
            \mix, Pwhite(0.5, 1.0),
            \lag, 0.1,
        ]);
        fxChain.add(\jpverb,[
            \revtime, Pwhite(1.0, 4.0),
            \mix, Pwhite(0.1, 0.4),
        ]);
    }

    play {
        fxChain.play;
        grainDelay.play;
    }

    free {
        fxChain.free;
        grainDelay.free;
    }
}
